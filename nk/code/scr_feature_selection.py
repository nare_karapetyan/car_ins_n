import numpy as np
import matplotlib.pyplot as plt
import math

###############################################################################
# Plot feature importance
# make importances relative to max importance
#   Arguments:
# \var feature_importance (numpy.ndarray)
# \var msg (string) is information about what type of method was used for finding the features. Example: msg = 'Importance based on SelectKBest with Chi2'
# \var the names of the (indexed) columns/features (numpy.ndarray)

def plot_feature_importance(feature_importance, title, cols, k):
    feature_importance = 100.0 * (feature_importance / feature_importance.max())
    #feature_importance = feature_importance[feature_importance !='nan']
    sorted_idx = np.argsort(feature_importance)
    pos = np.arange(sorted_idx.shape[0]) + .5
    plt.subplot(1, 2, 2)
    plt.barh(pos, feature_importance[sorted_idx], align='center')
    plt.yticks(pos, cols[sorted_idx])
    plt.xlabel('Relative Importance')
    plt.title(title)
    plt.show()
