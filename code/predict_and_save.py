import csv
from math import log
import numpy as np
from array import array
import pandas as pd
import math
import matplotlib.pyplot as plt

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn import datasets, linear_model
from sklearn.externals import joblib

import ntpath # for extracting the filename

import glob # for processing all the

predicate_dir = '../predicates/'
cleaned_test_data_dir = '../data/cleaned_test_portfolios/'
outfile = 'porfolio_predictions.csv'

features = ['SYS_Renewed', 'Vehicle_Passive_Restraint',
 'Vehicle_Collision_Coverage_Deductible',
 'EEA_Liability_Coverage_Only_Indicator',
 'Vehicle_Collision_Coverage_Indicator', 'Vehicle_Age_In_Years',
 'Vehicle_Make_Year', 'Vehicle_Symbol', 'Annual_Premium',
 'EEA_Full_Coverage_Indicator']

predicted_ratios = []
portf_num =[]
columns = ['Portfolio Number', 'Predicted Loss Ratio']
predicate_file = 'adaboost_1926_0.0057.pkl'

for i in range(1,601):
    print i
    curr_file = cleaned_test_data_dir + 'test_portfolio_'+str(i)+'.csv'
    df = pd.read_csv(curr_file)
    regr = joblib.load(predicate_dir + predicate_file )
    print '------------'
    print regr.get_params()
    print '------------'
    total_premium = df['Original_Annual_Premium'].sum()
    total_loss =  regr.predict(df[features]).sum()
    ratio = total_loss/total_premium
    predicted_ratios.append(ratio)
    portf_num.append(i)
final_df = pd.DataFrame(data={'Portfolio Number': portf_num, 'Predicted Loss Ratio': predicted_ratios})
final_df.to_csv(outfile, index=False)
